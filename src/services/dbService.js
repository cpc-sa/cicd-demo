var mysql = require("mysql");
const toUnnamed = require("named-placeholders")();
const LogModel = require("../logs/log-model");
const log = require("../logs/logger");

var config = require("../config/config");
var pool = null;
function createPool() {
  pool = mysql.createPool({
    connectionLimit: 10,
    host: config.db_endpoint,
    user: config.db_user,
    password: config.db_password,
    database: config.db_name
  });
}

function query(sql, param = {}, logmodel = new LogModel()) {
  if (pool === null) {
    createPool();
  }
  return new Promise((resolve, reject) => {
    var q = toUnnamed(sql, param);
    logmodel.setRequest(q[0], q[1]);
    pool.query(q[0], q[1], function(error, results, fields) {
      if (error) {
        reject(error);
      }
      logmodel.setResponse(results, 200);
      log.debug("db service", logmodel);
      resolve(results);
    });
  });
}

module.exports.query = query;

function queryProbe(sql) {
  if (pool === null) {
    createPool();
  }
  return new Promise((resolve, reject) => {
    pool.query(sql, function(error, results, fields) {
      if (error) {
        reject(error);
      }
      resolve(results);
    });
  });
}

module.exports.queryProbe = queryProbe;
