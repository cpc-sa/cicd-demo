var express = require("express");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var pretty = require("express-prettify");
var indexRouter = require("./controllers/index");
var logdemo = require("./controllers/log-demo");
var app = express();
const serverHealth = require("server-health");
const promMid = require('express-prometheus-middleware');
//const dbService = require("./services/dbService");

app.use(pretty({ query: "pretty" }));

app.use(promMid({
  metricsPath: '/metrics',
  collectDefaultMetrics: true,
  requestDurationBuckets: [0.1, 0.5, 1, 1.5],
}));

serverHealth.addConnectionCheck("database", async function() {
  try {
 //   await dbService.queryProbe("select 1 ");
    return true;
  } catch (e) {
    return false;
  }
});
serverHealth.exposeHealthEndpoint(app, "/health", "express");

if (process.env.NODE_ENV === "test") {
  app.use(logger("combined"));
}

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use("/", indexRouter);
app.use("/log", logdemo);

var port = process.env.PORT || "3000";
app.listen(port);

module.exports = app;
